using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Trabalho.Models;

namespace Trabalho.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]

    public class VisualizacoesController : ControllerBase
    {
        private Visualizacoes _vizualizacao;

        private void visualizacoes()
        {
            _vizualizacao = new Visualizacoes();

        }

        [Route("visualizacoes")]
        [HttpGet]
        public IActionResult Getvisualizacoes()
        {
            return Ok(_vizualizacao.visualizacoes.Where(x => x.Ativo == true).ToList());
        }

        [Route("{id}")]
        [HttpGet]
        public IActionResult GetVizulizacao(int id)
        {
            return Ok(_vizualizacao.visualizacoes.Where(x => x.Id == id).FirstOrDefault());
        }

        [HttpPost]
        public IActionResult Post([FromBodyAttribute] Visualizacoes novoVisualizacao)
        {
            try
            {
                _vizualizacao.visualizacoes.Add(novoVisualizacao);
                return Ok(_vizualizacao.visualizacoes.FirstOrDefault());

            }
            catch (Exception e)
            {
                return BadRequest(ModelState.IsValid);
            }


        }

        [HttpPut]
        public IActionResult Put([FromBodyAttribute] dynamic updateVisualizacao)
        {

            var usuarioExiste = _vizualizacao.visualizacoes.Where(x => x.Id == updateVisualizacao.id).FirstOrDefault();
            if (string.IsNullOrEmpty(usuarioExiste.ToString()))
            {
                try
                {
                    usuarioExiste.Nome = updateVisualizacao.nome;
                    return Ok(usuarioExiste.ToString());
                }
                catch (Exception e)
                {
                    return BadRequest(e.GetBaseException());
                }

            }


            return BadRequest();
        }

        [Route("{id}")]
        [HttpDelete]
        public IActionResult Delete(int id)
        {


            var visualizacoesExiste = _vizualizacao.visualizacoes.Where(x => x.Id == id).FirstOrDefault();

            if (string.IsNullOrEmpty(visualizacoesExiste.ToString()))
            {
                try
                {
                    _vizualizacao.visualizacoes.Remove(visualizacoesExiste);
                    return Ok("{status: Sucesso, mensagem: Deletado Com Sucesso, dados:{} }");
                }
                catch (Exception e)
                {
                    return BadRequest(e.GetBaseException());
                }

            }


            return BadRequest("{status: Falha, mensagem: Parametro id do usuario necessario, dados:{parametros [{id: Usuario id}]} }");
        }
    }
}
