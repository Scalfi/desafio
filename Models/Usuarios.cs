using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Trabalho.Models
{
    public class Usuarios
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Nome é necessário"), MinLength(3)]
        public string Nome { get; set; }
        
        [Required(ErrorMessage = "Senha é necessário")]
        public string Senha { get; set; }
       
        [Required(ErrorMessage = "Login é necessário")]
        public string Login { get; set; }
        
        [Required(ErrorMessage = "Email é necessário")]
        public string Email { get; set; }

        public DateTime Create_at { get; set; }
        public DateTime Update_at { get; set; }

        public GrupoVizualizacoes GrupoVizualizacoes {get; set;}

        public List<Usuarios> usuarios {get; set;}
    }
}
