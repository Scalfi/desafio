using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Trabalho.Models;

namespace Trabalho.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]

    public class GrupoVizualizacoesController : ControllerBase
    {
        private GrupoVizualizacoes _grupo;

        private void GrupoVizualizacoes()
        {
            _grupo = new GrupoVizualizacoes();

        }
        [Route("grupos")]
        [HttpGet]
        public IActionResult GetVizualizacoes()
        {
            return Ok(_grupo.Grupo.Where(x => x.Ativo == true).ToList());
        }

        [Route("{id}")]
        [HttpGet]
        public IActionResult GetVizulizacao(int id)
        {
            return Ok(_grupo.Grupo.Where(x => x.Id == id).FirstOrDefault());
        }

        [HttpPost]
        public IActionResult Post([FromBodyAttribute] GrupoVizualizacoes novoGrupo)
        {
            try
            {
                _grupo.Grupo.Add(novoGrupo);
                return Ok(_grupo.Grupo.FirstOrDefault());

            }
            catch (Exception e)
            {
                return BadRequest(ModelState.IsValid);
            }


        }

        [HttpPut]
        public IActionResult Put([FromBodyAttribute] dynamic updateGrupo)
        {

            var usuarioExiste = _grupo.Grupo.Where(x => x.Id == updateGrupo.id).FirstOrDefault();
            if (string.IsNullOrEmpty(usuarioExiste.ToString()))
            {
                try
                {
                    usuarioExiste.Nome = updateGrupo.nome;
                    return Ok(usuarioExiste.ToString());
                }
                catch (Exception e)
                {
                    return BadRequest(e.GetBaseException());
                }

            }


            return BadRequest();
        }

        [Route("{id}")]
        [HttpDelete]
        public IActionResult Delete(int id)
        {


            var grupoExiste = _grupo.Grupo.Where(x => x.Id == id).FirstOrDefault();

            if (string.IsNullOrEmpty(grupoExiste.ToString()))
            {
                try
                {
                    _grupo.Grupo.Remove(grupoExiste);
                    return Ok("{status: Sucesso, mensagem: Deletado Com Sucesso, dados:{} }");
                }
                catch (Exception e)
                {
                    return BadRequest(e.GetBaseException());
                }

            }


            return BadRequest("{status: Falha, mensagem: Parametro id do usuario necessario, dados:{parametros [{id: Usuario id}]} }");
        }
    }
}
