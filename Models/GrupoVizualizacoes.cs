using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trabalho.Models
{
    public class GrupoVizualizacoes
    {
        public int Id { get; set; }

        public string Nome { get; set; }
        
        public bool Ativo { get; set; }
        public DateTime Create_at { get; set; }
        public DateTime Update_at { get; set; }

        public List<Visualizacoes> Vizualizacoesa { get; set; }

        public List<GrupoVizualizacoes> Grupo { get; set; }

    }
}
