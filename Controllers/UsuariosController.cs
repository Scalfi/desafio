using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Trabalho.Models;

namespace Trabalho.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]

    public class USuariosController : ControllerBase
    {
        private Usuarios _usuarios;

        private USuariosController()
        {
            _usuarios = new Usuarios();
        }

        [Route("usuarios")]
        [HttpGet]
        public IActionResult GetUsuarios()
        {
            return Ok(_usuarios.usuarios.ToList());
        }

        [Route("{id}")]
        [HttpGet]
        public IActionResult GetUsuario(int id)
        {
            return Ok(_usuarios.usuarios.Where(x => x.Id == id).FirstOrDefault());
        }

        [HttpPost]
        public IActionResult Post([FromBodyAttribute] Usuarios novoUsuario)
        {
            try
            {
                _usuarios.usuarios.Add(novoUsuario);
                return Ok(_usuarios.usuarios.FirstOrDefault());

            }
            catch (Exception e)
            {
                return BadRequest(ModelState.IsValid);
            }


        }

        [HttpPut]
        public IActionResult Put([FromBodyAttribute] dynamic updateUsuario)
        {

            var usuarioExiste = _usuarios.usuarios.Where(x => x.Id == updateUsuario.id).FirstOrDefault();
            if (string.IsNullOrEmpty(usuarioExiste.ToString()))
            {
                try
                {
                    usuarioExiste.Nome = updateUsuario.nome;
                    return Ok(usuarioExiste.ToString());
                }
                catch (Exception e)
                {
                    return BadRequest(e.GetBaseException());
                }

            }


            return BadRequest();
        }

        [Route("{id}")]
        [HttpDelete]
        public IActionResult Delete(int id)
        {


            var usuarioExiste = _usuarios.usuarios.Where(x => x.Id == id).FirstOrDefault();

            if (string.IsNullOrEmpty(usuarioExiste.ToString()))
            {
                try
                {

                    return Ok("{status: Sucesso, mensagem: Deletado Com Sucesso, dados:{} }");
                }
                catch (Exception e)
                {
                    return BadRequest(e.GetBaseException());
                }

            }


            return BadRequest("{status: Falha, mensagem: Parametro id do usuario necessario, dados:{parametros [{id: Usuario id}]} }");
        }
    }
}
